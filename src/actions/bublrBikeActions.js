import * as axios from 'axios';

/*return info for all milwaukee stations*/
export async function getStationInfo() {
    let response = await axios.get('https://gbfs.bcycle.com/bcycle_bublr/station_information.json');
    return response.data.data;
}

export async function getStationStatus() {
    let response = await axios.get('https://gbfs.bcycle.com/bcycle_bublr/station_status.json');
    return response.data.data;
}
export async function getNumberAvailableBikes(station_id){
    return await getStationStatus().then(data => {
            return data.stations.filter(x => x.station_id === station_id);
        }
    )
}
export function getBikesWithinRadius(lat, long, radius){
    return getDistanceFromEachStation(lat, long).then(data => {
        return data.filter(x => x.distance < radius);
    });
}

async function getDistanceFromEachStation(lat, long ) {
   const status = await getStationStatus();
   return getStationInfo().then(data => {
       return data.stations.map(x => {
         return {
             address: x.address,
             station_id: x.station_id,
             distance: calculateDistance(lat, long, x.lat, x.lon),
             lat: x.lat,
             lng: x.lon,
             available: status.stations.filter(y => y.station_id === x.station_id)[0].num_bikes_available,
             dock: status.stations.filter(y => y.station_id === x.station_id)[0].num_docks_available,
           }
       });
   });
}
/* formula taken from https://www.movable-type.co.uk/scripts/latlong.html
* returns distance in kilometers*/
export function calculateDistance(lat, long, stationLat, stationLong){
    var R = 6368e3; // metres, earths radius
    var φ1 = deg2rad(lat);
    var φ2 = deg2rad(stationLat);
    var Δφ = deg2rad(stationLat-lat);
    var Δλ = deg2rad(stationLong-long);

    var a = Math.sin(Δφ/2) * Math.sin(Δφ/2) +
        Math.cos(φ1) * Math.cos(φ2) *
        Math.sin(Δλ/2) * Math.sin(Δλ/2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));

    var d = R * c;
    return d/1000;
}

// convert degrees to radians
function deg2rad(deg) {
    let rad = deg * Math.PI/180; // radians = degrees * pi/180
    return rad;
}
