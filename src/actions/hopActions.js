import * as axios from 'axios';

/*return info for all milwaukee stations*/
export async function getStopInfo() {
  let response = await axios.get(
    'https://transloc-api-1-2.p.rapidapi.com/stops.json?callback=call&geo_area=35.80176%2C-78.64347%7C35.78061%2C-78.68218&agencies=12%2C16',
    {
      headers: {
      'X-RapidAPI-Key': 'J2FzphjHBBmshBkyLfzX7zYUvO07p18Zx3mjsnXsYUdBfven21',
        'X-RapidAPI-Host': 'transloc-api-1-2.p.rapidapi.com'
      }
    });
  return response.data.data;
}


