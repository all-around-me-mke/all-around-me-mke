import ApolloClient from "apollo-boost";

const client = new ApolloClient({
  uri: "https://api.graph.cool/simple/v1/cjv9ndkca104i0187v4a45xpg"
});

export default client;
