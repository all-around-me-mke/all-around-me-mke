import React, {Component} from 'react';
import { Link } from 'react-router-dom';

class Marker extends Component {
  state = { show: false };

  toggleModal = () => {
    this.setState({ show: !this.state.show });
  };

  render() {
    const { icon, data, isBike } = this.props;
    if (!data) {
      return (
        <div>
          <img src={icon} onClick={this.toggleModal} className={"marker"}/>
          <div className={"marker-text"} style={{ display: this.state.show ? 'block' : 'none' }}>
            <p>You Are Here</p>
          </div>
        </div>
      )
    }
    if (isBike) {
      return (
        <div>
            <img src={icon} onClick={this.toggleModal} className={"marker"}/>
          <div className={"marker-text"} style={{ display: this.state.show ? 'block' : 'none' }}>
            <h6>{data.address}</h6>
            <p >{`${data.available} bikes available` }</p>
            <p>{ `${data.dock} docks available` }</p>
          </div>
        </div>
      )
    }
    return (
      <div>
          <img src={icon} onClick={this.toggleModal}  className={"marker"}/>
          <div className={"marker-text"} style={{ display: this.state.show ? 'block' : 'none' }}>
            <h6>{data.title}</h6>
            <p>{data.address}</p>
            <Link to={ (data && data.id) ? `/details/${data.id}` : ''}>More Details</Link>
          </div>
      </div>
    )

  }
}


export default Marker;
