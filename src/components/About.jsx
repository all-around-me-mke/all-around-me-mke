import React, { Component } from 'react';
import { Button, Row, Col, Container} from 'react-bootstrap';
import MenuBar from './MenuBar';

class About extends Component {

    render() {
        return (
            <div>
                <MenuBar />
                <div className="splash-background" style={{ height: '100vh', width: '100%' }}>
                  <Container  className={"detail-container"}>
                    <h1><img src={require('../images/flag-seal-color-small.png')}></img></h1>
                        <h1 style={{ color: 'Black' }}>All Around Me MKE </h1>
                        <p><b>Our team first came up with this idea at hack a pipeline hosted by Lift Up Careers. We designed this application
                            thinking of those new to Milwaukee or here as a tourist. Our goal is to create a centralized application that includes both the bublur
                            bikes and the hop as a mode of transportation, and include real-time events, historical sites, local coffee shops and local breweries near the users location.
                            It will truley give the user a true taste of all that Milwaukeians enjoy!</b>
                        </p>
                    </Container>
                </div>
            </div>
        );
    }
}

export default About;
