import React, {Component} from 'react';
import gql from "graphql-tag";
import moment from "moment";
import { graphql } from "react-apollo/index";
import { Button, Row, Col, Container } from 'react-bootstrap';
import icon from '../images/map-icon.png';
import bikeIcon from '../images/bike.png';
import Marker from './Marker';
import MenuBar from './MenuBar';
import GoogleMapReact from 'google-map-react';
import {  getBikesWithinRadius } from '../actions/bublrBikeActions';


const imageStyle = {
  "height": "auto",
  "width": "300px",
  "-o-object-fit": "contain",
};
class Details extends Component {
  state = {
    data: null,
    bikes: [],
  };

  componentDidMount() {
    if (this.props.data.allEverythings) {
      this.getData();
    }
  }

  componentDidUpdate(prevProps) {
    if ((this.props.data.allEverythings && this.props.data.allEverythings.length > 0 && prevProps.data !== this.props.data)
      || this.props.match.params.type !== prevProps.match.params.type
    ) {
      this.getData();
    }
  }

  getBikes = () => {
    getBikesWithinRadius(this.state.data.lat, this.state.data.lng, 0.5).then(data => {
      this.setState({ bikes: data })
    });
  };

  getData = () => {
    const data = this.props.data.allEverythings.filter(d => d.id === this.props.match.params.id);
    this.setState({
      data: data.length > 0 ? data[0] : null
    }, this.getBikes)
  };

  formatDate = date => {
    if (date) {
      return moment(date).format('LL');
    } else {
      return null;
    }
  };

  render() {
      const { data } = this.state;

      if (this.state.data) {
        return (
          <div>
            <MenuBar />
            <div style={{ height: '100vh', width: '100%', background: 'ghostwhite'}}>
              <Container className="detail-container">
                <img src={data.image} style={imageStyle} />
                <Row className="main-row">
                  <Col>
                    <h3>{data.title}</h3>
                    {
                      (data.openTime && data.startDate) ?
                        <p>{`${this.formatDate(data.startDate)} - ${this.formatDate(data.endDate)}`}</p> : null
                    }
                    {
                      data.openTime ?
                        <p>{`${data.openTime} - ${data.endTime}`}</p> : null
                    }
                    <p style={{"textAlign": "left"}}>&nbsp; &nbsp; {data.description}</p>
                  </Col>
                </Row>
              </Container>
              <div style={{ height: '30vh', width: '100%' }}>
                <h6>{`Bikes Near ${data.title}`}</h6>
                <GoogleMapReact
                  bootstrapURLKeys={{ key: process.env.REACT_APP_GOOGLE_API_KEY }}
                  center={[data.lat, data.lng]}
                  defaultZoom={15}
                  layerTypes={['BicyclingLayer']}
                >

                  {
                    this.state.bikes.map(bike => (
                      <Marker
                        lat={bike.lat || ''}
                        lng={bike.lng || ''}
                        icon={bikeIcon}
                        data={bike}
                        isBike={true}
                        className={'class'}
                      />
                    ))
                  }
                  <Marker
                    lat={data.lat || ''}
                    lng={data.lng || ''}
                    icon={icon}
                    isBike={true}
                    data={data}
                  />
                </GoogleMapReact>
              </div>
            </div>
          </div>
        )
      }
      return null;
  }
}

const GET_THING = gql`
  query {
    allEverythings {
      id,
      title,
      startDate,
      description,
      endDate,
      address,
      image,
      lat,
      lng,
      openTime,
      endTime,
      type
    }
  }
`;

export default graphql(GET_THING)(Details);
