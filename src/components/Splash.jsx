import React, { Component } from 'react';
import { Image, Container, Col, Row, Button } from 'react-bootstrap';

export default class Splash extends Component {
    render() {
        return (
            <div className="splash-background" style={{ height: '100vh', width: '100%' }}>
                <Container>
                    <Row className="splash-header">
                        <Col>
                          <h1><strong>All Around Me MKE</strong></h1>
                        </Col>
                    </Row>
                    <Row className="splash-image">
                        <Image src={require('../images/flag-seal-color.png')} ></Image>
                    </Row>
                    <Row>
                        <Col>
                            <Button href="/main" className="navy-button" size="lg" block>Get Started</Button>
                        </Col>
                    </Row>
                </Container>
            </div>)
    }
}
