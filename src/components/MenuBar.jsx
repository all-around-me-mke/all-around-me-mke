import React from 'react';
import { slide as Menu } from 'react-burger-menu';
import {Row} from "react-bootstrap";

const MenuBar = () => (
  <div>
      <h4 className={'header'}>All Around Me Mke</h4>
      <Menu>
      <a id="home" className="menu-item" href="/home">Home</a>
      <a id="about" className="menu-item" href="/about">About</a>
      <a id="contact" className="menu-item" href="/swag">Swag</a>
      <a id="contact" className="menu-item" href="/contact">Contact</a>
    </Menu>
  </div>
);

export default MenuBar;
