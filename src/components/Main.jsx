import React, { Component } from 'react';
import { Button, Row, Col, Container, Card } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import MenuBar from './MenuBar';

class Main extends Component {

    render() {
        return (
            <div>
                <MenuBar />
                <div className="background-image" style={{ height: '100vh', width: '100%' }}>
                    <Container className="main-container">
                        <Row className="main-row">
                            <Col>
                                <Link to={"map/event"} >
                                  <Card className="navy-button" text="white" >
                                    <Card.Body>
                                      <Card.Title>Events</Card.Title>
                                    </Card.Body>
                                  </Card>
                                </Link>
                            </Col>
                        </Row>
                        <Row className="main-row">
                            <Col>
                              <Link to={"map/historical"} >
                                <Card className="navy-button" text="white" >
                                  <Card.Body>
                                    <Card.Title>Historical Locations</Card.Title>
                                  </Card.Body>
                                </Card>
                              </Link>
                            </Col>
                        </Row>
                        <Row className="main-row">
                            <Col>
                              <Link to={"map/brewery"} >
                                <Card className="navy-button" text="white" >
                                  <Card.Body>
                                    <Card.Title>Breweries</Card.Title>
                                  </Card.Body>
                                </Card>
                              </Link>
                            </Col>
                        </Row>
                        <Row className="main-row">
                            <Col>
                              <Link to={"map/coffee"} >
                                <Card className="navy-button" text="white" >
                                  <Card.Body>
                                    <Card.Title>Coffee Shops</Card.Title>
                                  </Card.Body>
                                </Card>
                              </Link>
                            </Col>
                        </Row>
                    </Container>
                </div>
            </div>
        );
    }
}

export default Main;
