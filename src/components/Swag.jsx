import React, { Component } from 'react';
import { Button, Row, Col, Container, Image } from 'react-bootstrap';
import MenuBar from './MenuBar';

class Swag extends Component {

    render() {
        return (
            <div>
                <MenuBar />
                <div className="splash-background" style={{ height: '100vh', width: '100%' }}>
                    <Container className={"detail-container"}>
                        <h1 style={{ color: 'Black' }}>Future Store for Vel Phillips Statue </h1>
                        <Image src={require('../images/vel.jpeg')} ></Image>
                    </Container>
                </div>
            </div>
        );
    }
}

export default Swag;
