import React, { Component } from 'react';
import GoogleMapReact from 'google-map-react';
import { graphql } from "react-apollo";
import gql from 'graphql-tag';
import Marker from './Marker';
import icon from '../images/map-icon.png';
import bikeIcon from '../images/bike.png';
import here from '../images/here.png';
import MenuBar from './MenuBar';
import { Button, Row, Col, Container, Spinner } from 'react-bootstrap';
import { calculateDistance, getBikesWithinRadius } from '../actions/bublrBikeActions';

class Map extends Component {
  state = {
    center: {
      lat: null,
      lng: null
    },
    things: [],
    bikes: [],
  };

  componentDidMount() {
    this.getLocation();
    if (this.props.data.allEverythings) {
      this.filterThings();
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if ((this.props.data.allEverythings && prevProps.data.allEverythings !== this.props.data.allEverythings)
      || this.props.match.params.type !== prevProps.match.params.type
    ) {
      this.filterThings();
    }
    if (prevState.center !== this.state.center) {
      this.getBikes();
    }
  }

  getBikes = () => {
    getBikesWithinRadius(this.state.center.lat, this.state.center.lng, 0.5).then(data => {
      this.setState({ bikes: data })
    });
  };

  filterThings = () => {
    this.setState({
      things: this.props.data.allEverythings.filter(d => d.type === this.props.match.params.type)
    });
  };

  getLocation = () => {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(position => {
        this.setState({
          center: {
            lat: position.coords.latitude,
            lng: position.coords.longitude,
          }
        })
      }, this.getBikes)
    }
  };

  render() {
    if (this.state.center.lat && this.state.things.length) {
      return (
        <Container style={{paddingLeft: 0, background: 'ghostwhite' }}>
          <Row>
            ]          <MenuBar />
            <h4 className={'header'}><strong>All Around Me Mke</strong></h4>
          </Row>
          <Row className="map-padding" >
            <div style={{ height: '80vh', width: '100%' }}>
              <GoogleMapReact
                bootstrapURLKeys={{ key: process.env.REACT_APP_GOOGLE_API_KEY }}
                center={[this.state.center.lat, this.state.center.lng]}
                defaultCenter={[43.0389, -87.9065]}
                defaultZoom={15}
                layerTypes={['BicyclingLayer']}
              >
                {
                  this.state.center.lat ?
                    <Marker
                      lat={this.state.center.lat || ''}
                      lng={this.state.center.lng || ''}
                      icon={here}
                    /> : null
                }
                {
                  this.state.things.map(thing => (
                    <Marker
                      lat={thing.lat || ''}
                      lng={thing.lng || ''}
                      icon={icon}
                      data={thing}
                    />
                  ))
                }
                {
                  this.state.bikes.map(bike => (
                    <Marker
                      lat={bike.lat || ''}
                      lng={bike.lng || ''}
                      icon={bikeIcon}
                      data={bike}
                      isBike={true}
                      className={'class'}
                    />
                  ))
                }
              </GoogleMapReact>
            </div>
          </Row>
          <Row>
            <Col style={{padding: '10px'}}>
              <Button href="/main" variant="secondary">Back</Button>
            </Col>
          </Row>
        </Container>
      );
    }
    return (
      <Container style={{paddingLeft: 0, background: 'ghostwhite' }}>
        <Row>
]          <MenuBar />
           <h4 className={'header'}><strong>All Around Me Mke</strong></h4>
        </Row>
        <Row className="map-padding" >
          <div style={{ height: '80vh', width: '100%' }} className={"loading"}>
            <Spinner animation="grow" className="spinner" />
            <Spinner animation="grow" className="spinner" />
            <Spinner animation="grow" className="spinner" />
          </div>
        </Row>
        <Row>
          <Col style={{padding: '10px'}}>
            <Button href="/main" variant="secondary">Back</Button>
          </Col>
        </Row>
      </Container>
    );
  }
}
const GET_THING = gql`
  query {
    allEverythings {
      id,
      title,
      startDate,
      endDate,
      address,
      image,
      lat,
      lng,
      openTime,
      endTime,
      type
    }
  }
`;

export default graphql(GET_THING)(Map);
