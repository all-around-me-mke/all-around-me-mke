import React, { Component } from 'react';
import { Button, Row, Col, Container} from 'react-bootstrap';
import MenuBar from './MenuBar';

class About extends Component {

    render() {
        return (
            <div>
                <MenuBar />
                <div className="splash-background" style={{ height: '100vh', width: '100%' }}>
                  <Container  className={"detail-container"} style={{ color: 'Black' }}>
                    <h1><img src={require('../images/flag-seal-color-small.png')}></img></h1>
                        <h1 >All Around Me MKE </h1>
                        <p><b>Do you have any recommendation or feedback to give to the team? If so, we would love to hear your feedback
                            on how to make the user experience better! Email: allaboutmemke@gmail.com
                        </b>
                        </p>
                    </Container>
                </div>
            </div>
        );
    }
}

export default About;
