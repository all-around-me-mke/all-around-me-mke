import React from 'react';
import { ApolloProvider } from 'react-apollo';
import logo from './logo.svg';
import './App.scss';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import Splash from './components/Splash';
import Main from './components/Main';
import Details from './components/Details';
import Map from './components/Map';
import client from './actions/ApolloClient';
import About from './components/About';
import Swag from './components/Swag';
import Contact from './components/Contact';

function App() {
  return (
    <div className="App">
      <ApolloProvider client={client}>
        <Router>
          <div>
            <Route path="/home" component={Main} />
            <Route path="/main/" component={Main} />
            <Route path="/about" component={About} />
            <Route path="/swag" component={Swag} />
            <Route path="/contact" component={Contact} />
            <Route path="/details/:id" component={Details} />
            <Route path="/map/:type" component={Map} />
            <Route exact path="/" component={Splash} />
          </div>
        </Router>
      </ApolloProvider>
    </div>
  );
}

export default App;
